import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1

Rectangle {
    id: loginpage
    ColumnLayout {
        id: columnLayout
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: parent.left
        spacing: 10

        TextField {
            id: textField1
            placeholderText:  qsTr("User Name")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        TextField {
            id: textField2
            placeholderText:  qsTr("Password")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        TextField {
            id: textField3
            placeholderText:  qsTr("SSID")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        Rectangle {
            id: loginRect
            width: 200
            height: 60
            color: loginButton.pressed ? "#a02c63a6" : "#2c63a6"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            MouseArea {
                id: loginButton
                height: 60
                anchors.fill: parent
                onClicked: {
                    focus: true
                    busyIndicator.visible = true

                    if(tclient.connect())
                    {
                        //connected
                        root.state = "ControlState"

                    }
                    else{
                        //connection failed
                        root.state  = "LoginState"
                    }
                    busyIndicator.visible = false
                }
            }

            Label {
                id: label
                height: 60
                text: qsTr("Login")
                font.letterSpacing: 0
                padding: 2
                font.bold: true
                font.pointSize: 20
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        x: 290
        y: 180
        z: 1
        visible: false
    }
}
