import QtQuick 2.0
import QtQuick.Controls 2.1



Item {
    id: controlnew


    Column {
        id: column
        x: 58
        y: 45
        anchors.horizontalCenterOffset: 0
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter

        Rectangle {
            id: colorRect
            width: 200
            height: 50
            color: "#ffffff"
            radius: 10
        }

        Row {
            id: row
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 30

            Slider {
                id: greenSlider
                to: 1024
                orientation: Qt.Vertical
                value: 0
                stepSize: 1




            }

            Slider {
                id: blueSlider
                to: 1024
                orientation: Qt.Vertical
                value: 0
                stepSize: 1

            }

            Slider {
                id: redSlider
                to: 1024
                orientation: Qt.Vertical
                stepSize: 1

                value: 0
            }


        }

        Dial {
            id: dial
            width: 100
            height: 100
            anchors.horizontalCenter: parent.horizontalCenter
        }


    }
}
