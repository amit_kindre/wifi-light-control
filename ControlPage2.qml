import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
Item {
    id: item1

    ColumnLayout {
        id: columnLayout
        x: 93
        y: 147
        width: 200
        height: 344
        spacing: 5
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter


        Rectangle {
            id: rectangle
            width: 200
            height: 50
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#c0ffffff"
                }

                GradientStop {
                    position: 1
                    color: "#000000"
                }
            }

            Slider {
                id: slider
                anchors.fill: parent
                value: 0
                to: 1024.0
                stepSize: 8

                live : true
                snapMode: Slider.SnapAlways

                orientation: Qt.Horizontal

                onValueChanged: {
                    tclient.brightness(slider.value, "WHI");

                    console.log(slider.value);
                }
            }
        }

        Rectangle {
            id: rectangle1
            width: 200
            height: 50
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#c0eeee36"
                }

                GradientStop {
                    position: 1
                    color: "#e0fcfccc"
                }
            }




            Slider {
                id: slider1
                height: 50
                anchors.fill: parent
                value: 0
                to: 1024.0
                stepSize: 8

                live : true
                snapMode: Slider.SnapAlways

                orientation: Qt.Horizontal

                onValueChanged: {
                    tclient.brightness(slider1.value, "WRM");

                    console.log(slider1.value);
                }
            }
        }

        Switch {
            id: switch1
            text: qsTr("Switch")

        }
    }

}
