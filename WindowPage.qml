import QtQuick 2.0

Rectangle {
    id: root

    property alias myMenuBar: myMenuBar
    property alias loginxp: loginxp


    Loginxp{
        id: loginxp
        anchors.fill: parent
    }


    Cntrlpage{
        id: controlPage
        anchors.fill: parent
        visible: false
    }

    MyMenuBar {
        id: myMenuBar
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
    }

    Rectangle {
    focus: true // important - otherwise we'll get no key events

    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            console.log("Back button captured ")
            tclient.logoutuser();
            root.state = root.state=="ControlState" ? "LoginState" : Qt.quit()
            event.accepted = true
        }
    }
    }

    states: [
        State {
            name: "ControlState"

            PropertyChanges {
                target: loginxp
                visible: false
            }
            PropertyChanges{
                target: controlPage; visible: true;
            }
        },
        State {
            name: "LoginState"
        }
    ]

}
