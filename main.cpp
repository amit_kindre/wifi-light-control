#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>


#include "tcpclient.h"
#include "wifi.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    //QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    QQmlContext* contextp = engine.rootContext();

    TcpClient tClient;
    contextp->setContextProperty("tclient",&tClient); //Expose C++ function to qml



    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}


