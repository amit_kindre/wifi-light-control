import QtQuick 2.0
//import QtQuick.Controls 1.4 //for live vales
import QtQuick.Controls 2.1
//import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

Item {
    id: controlpage


    ColumnLayout {
        id: columnLayout
        x: 225
        y: 112
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Rectangle {
            id: rectangle3
            width: 200
            height: 50
            radius: 1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: 0
            border.color: "#7a7d85"
            border.width: 3
            anchors.horizontalCenter: parent.horizontalCenter





            Label {
                id: label
                text: qsTr("Color")
                anchors.fill: parent
                font.pointSize: 12
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        Row {
            id: row
            width: 200
            Layout.rowSpan: 1
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            anchors.topMargin: 20
            anchors.top: rectangle3.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20


            Rectangle {
                id: rectangle
                width: 50
                height: 200
                radius: 5
                border.width: 3
                rotation: 0
                //Rotation: 90

                gradient: Gradient {

                    GradientStop {
                        position: 1
                        color: "#c0ff0000"
                    }

                    GradientStop {
                        position: 0
                        color: "#e0ffffff"
                    }

                }

                Slider {
                    id: slider
                    anchors.bottomMargin: 3
                    anchors.rightMargin: 0
                    anchors.topMargin: 3
                    anchors.fill: parent


                    value: 0
                    to: 1024.0
                    stepSize: 8

                    live : true
                    snapMode: Slider.SnapAlways

                    orientation: Qt.Vertical

                    onValueChanged:   {
                        tclient.brightness(slider.value, "RED");
                        rectangle3.color= Qt.rgba((slider.value/1024.0),(slider1.value/1024.0),(slider2.value/1024.0),slider3.value/1024);
                        console.log(slider.value);
                    }
                }
            }

            Rectangle {
                id: rectangle1
                width: 50
                height: 200
                radius: 5
                border.width: 3
                rotation: 0
                gradient: Gradient {

                    GradientStop {
                        position: 1
                        color: "#c000ff00"
                    }

                    GradientStop {
                        position: 0
                        color: "#e0ffffff"
                    }

                }
                Slider {
                    id: slider1
                    anchors.bottomMargin: 3
                    anchors.topMargin: 3
                    anchors.fill: parent

                    value: 0
                    to: 1024.0
                    stepSize: 8

                    live : true
                    snapMode: Slider.SnapAlways

                    orientation: Qt.Vertical

                    onValueChanged:   {
                        tclient.brightness(slider1.value, "GRE");
                        rectangle3.color= Qt.rgba((slider1.value/1024.0),(slider1.value/1024.0),(slider2.value/1024.0),slider3.value/1024);
                        console.log(slider1.value);
                    }

                }
                //Rotation: 90
            }

            Rectangle {
                id: rectangle2
                width: 50
                height: 200
                radius: 5
                border.width: 3
                rotation: 0

                gradient: Gradient {

                    GradientStop {
                        position: 1
                        color: "#c00000ff"
                    }

                    GradientStop {
                        position: 0
                        color: "#e0ffffff"
                    }
                }

                Slider {
                    id: slider2
                    anchors.bottomMargin: 3
                    anchors.topMargin: 3
                    anchors.fill: parent
                    value: 0
                    to: 1024.0
                    stepSize: 8

                    live : true
                    snapMode: Slider.SnapAlways

                    orientation: Qt.Vertical

                    onValueChanged: {
                        tclient.brightness(slider2.value, "BLU");
                        rectangle3.color= Qt.rgba((slider.value/1024.0),(slider1.value/1024.0),(slider2.value/1024.0),slider3.value/1024);
                        console.log(slider2.value);
                    }
                }
                // Rotation: 90
            }
        }

        Rectangle {
            id: rectangle4
            width: 50
            height: 200
            radius: 1
            Layout.fillHeight: false
            Layout.preferredHeight: 0
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: 0
            anchors.top: row.bottom
            anchors.topMargin: -50
            anchors.horizontalCenter: parent.horizontalCenter
            border.width: 3
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#c0fdfdfd"
                }

                GradientStop {
                    position: 1
                    color: "#e0000000"
                }
            }
            rotation: 90

            Slider {
                id: slider3
                anchors.bottomMargin: 3
                anchors.topMargin: 3
                anchors.fill: parent
                orientation: Qt.Vertical
                stepSize: 8
                to : 1024
                value: 0
                live : true


                onValueChanged: {
                    tclient.brightness(slider3.value, "HUE");
                    rectangle3.color= Qt.rgba((slider.value/1024.0),(slider1.value/1024.0),(slider2.value/1024.0),slider3.value/1024);
                }
            }
        }
    }




}
